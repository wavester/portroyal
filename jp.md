[japanese]
==========

### [manga / 漫画]
- [dl-raw](https://dl-raw.net/)
- [raw lazy](https://rawlazy.com/)

### [general media / 図書館]
- [tokyo toshokan / 東京 図書館](https://www.tokyotosho.info/) A BitTorrent Library for Japanese Media
- [acgnX torrent](https://www.acgnx.se/) Anime Comics Game Novel eXchange
- [anidex](https://anidex.info/)
- [anirena](https://www.anirena.com/)
- [nyaa](https://nyaa.si/)

### [literature / 文献]
- [aozora](https://www.aozora.gr.jp/) インターネットの電子図書館、青空文庫へようこそ。

### [movies / 映画]
- [mega link - old movies](https://mega.nz/folder/adFgGTJJ#Il5P1pjZF04bFa6k-6pZDw)

### [studying resources / 研究リソース]
- [itazuraneko](https://itazuraneko.neocities.org/learn/guide.html) The suggest guide on how to start learning Japanese
- [tatsumoto](https://tatsumoto-ren.github.io/blog/resources.html) This is the Ajatt-Tools Resources List
- [mega link - pdf's, videos](https://mega.nz/folder/VR5k3QJS#ND_YA4QvRJ8j4orcgAUkWw)


### [dictionary / 辞書]
- [jisho](https://jisho.org/) Jisho is a powerful Japanese-English dictionary. It lets you find words, kanji, example sentences and more quickly and easily. 
- [weblio](https://www.weblio.jp/) 国語辞典
- [online japanese accent dictionary](http://www.gavo.t.u-tokyo.ac.jp/ojad/eng/pages/home)
