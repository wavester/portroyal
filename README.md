# [port royal]

An Markdown index built and maintained by the members of [deep dive] to make piracy easier to access.

## Ways to contribute

- By adding new sections that didn't exist yet.

- Fix errors in pre-existing sections or add improvements.

## How to contribute

- In order to contribute to [port royal], please read [Gitlab's guide on the workflow of Contribution](https://docs.gitlab.com/ee/development/contributing/#contribution-flow). If you do not merge request, we will be *unable* to add this to our site.

- Here you can see in GIF format how you can contribute and merge to our project.
## [Step 1] Fork

![fork](/uploads/8625c5e41ee69caa9376db2797526a10/fork.gif)

## [Step 2] Emerge Request

![merge-request](/uploads/dc3a8d40df30d228145c7ea2126a27c2/merge-request.gif)

## Rules for submission

- Make sure to not post any links that contain a virus or that are malicious in any other way.

- Do not submit duplicates or dead links. 


